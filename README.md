## Story Editor

- Based on React JS
- https://github.com/MrBlenny/react-flow-chart
- Preview: https://qurator-platform.gitlab.io/dfki/react-story-editor/


```
# Install dependencies
npm install

# Start dev server
npm start
```

## Settings

You can set the following settings as environment variable or edit the `.env` file.

```
REACT_APP_TITLE=Story Editor
REACT_APP_BASE_URL=http://localhost:3000
REACT_APP_API_ENDPOINT=http://localhost:8000/srv/text-classification
```

## Screencast

- Empty screen
- Init with topic
- Select topic 
- Drag segment
- Drag segment 2
- Delete segment
- Select segment
- Drag another segment 
- Empty
- Init topic
- Generate story
- Zoom in/out
- Create custom

> Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).

- Link custom to story
- Export



## License 

Internal use only
