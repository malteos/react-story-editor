import React from "react";
import {Navbar} from "@blueprintjs/core";
import {Alignment} from "@blueprintjs/core";

const title = document.documentElement.getAttribute('data-title');

class PageHeader extends React.Component {
    render() {
        return (
            <Navbar>
                <Navbar.Group align={Alignment.LEFT}>
                    <img src={`${process.env.PUBLIC_URL}/static/logo.png`} height="30" alt="Logo" />
                    <Navbar.Divider />
                    <Navbar.Heading>{title}</Navbar.Heading>
                </Navbar.Group>
            </Navbar>
        );
    }
}

export default PageHeader;
