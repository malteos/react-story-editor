import {cloneDeep, mapValues} from 'lodash'
import * as React from 'react'
import {
    FlowChart,
    IChart,
} from '@mrblenny/react-flow-chart/src'
import * as actions from '@mrblenny/react-flow-chart/src/container/actions'
import {
    Content,
    Page,
    Sidebar,
    NodeInnerCustom,
    NodeCustom,
} from './ChartComponents'
import InitializeTopicForm from "./InitializeTopicForm";
import {Button, ControlGroup, InputGroup, Spinner} from "@blueprintjs/core";
import {SelectedNode} from "./ChartComponents/SelectedNode";
import {SelectedLink} from "./ChartComponents/SelectedLink";
import {ExportStory} from "./ExportStory";
import {LinkCustom} from "./ChartComponents/LinkCustom";
import {customActions} from "./callbacks";
import {AppToaster} from "./toaster";
import CreateSegmentForm from "./CreateSegmentForm";
import {ISelectedOrHovered} from "@mrblenny/react-flow-chart/src/types/chart";
import {emptyChart} from "./misc/exampleChartState";
import {CanvasOuterCustom} from "./ChartComponents/CanvasOuterCustom";
// import {CreateSegmentForm} from "./CreateSegmentForm";

export class StoryChart extends React.Component<{chart: IChart}, any>  {
    public state: IChart;

    constructor(props: any) {
        super(props);

        const { chart } = props;
        // this.chart = chart;
        this.state = chart;

        console.log('Chart in constructor: ', chart);

        this.changeChart = this.changeChart.bind(this);
        this.selectOnChart = this.selectOnChart.bind(this);

    }

    private changeChart(changedChart: any) {
        console.log('Changed: ', changedChart);

        this.setState({
            properties: {
                loading: true,
            }
        });

        setTimeout(() => {
            changedChart.properties = {
                loading: false
            };
            this.setState(changedChart);
            AppToaster.show({ icon: "tick-circle", intent:  "success", message: "Story successfully generated." });
        }, 1500);
    }

    private selectOnChart(selected: ISelectedOrHovered) {
        this.setState({
            selected: selected
        });
    }

    public render () {
        // const { chart } = this.state;
        const chart = this.state;

        console.log('Chart in render: ', chart);

        // let clonedActions = cloneDeep(actions);
        // clonedActions.onCanvasDrop = onCanvasDropCustom;
        // let clonedActions = {
        //     onCanvasDrop: onCanvasDropCustom,
        //     onDragNode,
        //     onDragNodeStop,
        //     onDragCanvas,
        //     onDragCanvasStop,
        //     onLinkStart,
        //     onLinkMove,
        //     onLinkComplete,
        //     onLinkCancel,
        //     onLinkMouseEnter,
        //     onLinkMouseLeave,
        //     onLinkClick,
        //     onCanvasClick,
        //     onNodeMouseEnter,
        //     onNodeMouseLeave,
        //     onDeleteKey,
        //     onNodeClick,
        //     onNodeDoubleClick,
        //     onNodeSizeChange,
        //     onPortPositionChange,
        //     // onCanvasDrop,
        //     onZoomCanvas,
        // };
            // clonedActions.onCanvasDrop = onCanvasDropCustom;

        let stateActions = mapValues(customActions, (func: any) =>
            (...args: any) => this.setState(func(...args))) as typeof actions;


        // stateActions.onCanvasDrop = () => {
        //
        // };

        //onCanvasDrop

        return (
            <>
            <div className="story-controls">
                <div className="story-controls-left">
                    <InitializeTopicForm
                        handleChartChange={changedChart => this.setState(changedChart)}
                    />
                    <Button
                        icon="zoom-in"
                        onClick={() => {
                            this.setState({
                                scale: this.state.scale + 0.1 ,
                            });
                        }}
                    />
                    <Button
                        icon="zoom-out"
                        onClick={() => {
                            this.setState({
                                scale: this.state.scale - 0.1 ,
                            })
                        }}
                    />
                    <Button
                        icon="zoom-to-fit"
                        onClick={() => {
                            this.setState({
                                scale: 1.0,
                            })
                        }}
                    />
                    <Button
                        icon="reset"
                        onClick={() => {
                            this.setState(cloneDeep(emptyChart));
                        }}
                    />

                </div>
                <div className="story-controls-right">
                    <CreateSegmentForm
                        handleChartChange={changedChart => this.setState(changedChart)}
                        chart={chart}
                    />
                    <ExportStory chart={chart} />
                    <ControlGroup fill={true} vertical={false}>
                        <InputGroup leftIcon="search" placeholder="Search documents" />
                    </ControlGroup>
                </div>
            </div>
            <Page>
                <Content>
                    <FlowChart
                        chart={chart}
                        callbacks={stateActions}
                        Components={{
                            Node: NodeCustom,
                            NodeInner: NodeInnerCustom,
                            Link: LinkCustom,
                            CanvasOuter: CanvasOuterCustom,
                        }}
                    />
                </Content>
                <Sidebar>
                    {chart.properties && chart.properties.loading ? (
                        <>
                            <p>
                                <br />
                                <br />
                            </p>
                            <Spinner />
                        </>
                    ): (
                        <>
                        {chart.selected.type && chart.selected.type === 'node' && chart.selected.id && chart.nodes.hasOwnProperty(chart.selected.id) &&
                                (
                                    <SelectedNode
                                        nodeId={chart.selected.id}
                                        node={chart.nodes[chart.selected.id]}
                                        stateActions={stateActions}
                                        handleChartChange={this.changeChart}
                                        selectOnChart={this.selectOnChart}
                                    />
                                )
                        }
                        {chart.selected.type && chart.selected.type === 'link' && chart.selected.id && (
                            <SelectedLink
                            linkId={chart.selected.id}
                            link={chart.links[chart.selected.id]}
                            stateActions={stateActions}
                            handleChartChange={this.changeChart}
                            />
                        )}
                        {!chart.selected.type && (
                            <div className="no-selection">
                            Click on a node or link to see details.
                            </div>
                        )}
                    </>
                )}

                </Sidebar>
            </Page>
            </>
        )
    }
}
