import {IRecommendationItem} from "../ChartComponents/RecommendationItem";

export const defaultRecommendationItem: IRecommendationItem = {
    title: 'West Harbour ',
    subtitle: 'Initial planning for the construction of a large port in Berlin started ',
    description: 'West Harbour is a inland harbor in Berlin\'s Moabit district.',
    source: 'Wikinews'
};

export const defaultRecommendationItems: IRecommendationItem[] = [
    defaultRecommendationItem,
    {
        title: 'Deutsche Einheit',
        subtitle: 'Verkehrsprojekt Deutsche Einheit Nr. 17',
        description: 'Der Westhafen ist Bestandteil des aus ökologischen Gründen umstrittenen Verkehrsprojekts Deutsche Einheit Nr. 17. Mit dem Ausbau der Wasserstraßenverbindung Hannover–Magdeburg–Berlin können Binnenschiffe mit bis zu 2000 Tonnen und mit einer Abladetiefe von bis zu 2,80 Meter den Westhafen erreichen. ',
        source: 'Wikinews'
    },
    {
        title: 'Binnenschifferkirche',
        subtitle: 'Von 1968 bis 2009 befand',
        description: 'sich an der Westhafenstraße 1 eine Schiffer- und Hafenkirche in einem ehemaligen Ladengebäude. Sie ist Nachfolgerin einer von der Schiffergemeinde bis 1943 genutzten schwimmenden Kirche.',
        source: 'Wikinews'
    },
    defaultRecommendationItem,
    defaultRecommendationItem,
    ];
