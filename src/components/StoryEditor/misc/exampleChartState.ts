import {IChart} from "@mrblenny/react-flow-chart/src";

export const emptyChart: IChart = {
    offset: {
        x: 0,
        y: 0,
    },
    scale: 1,
    nodes: {},
    links: {},
    selected: {},
    hovered: {},
};

export const chartSimple: IChart = {
    offset: {
        x: 0,
        y: 0,
    },
    scale: 0.8,
    nodes: {
        moabit: {
            id: 'moabit',
            type: 'topic',
            position: {
                x: 300,
                y: 100,
            },
            ports: {
                port1: {
                    id: 'port1',
                    type: 'output',
                    properties: {
                        value: 'yes',
                    },
                }
            },
            properties: {
                title: 'Moabit',
                description: 'Ein Stadtteil von Berlin',
            }
        },
        node2: {
            id: 'node2',
            type: 'document',
            position: {
                x: 300,
                y: 300,
            },
            ports: {
                port1: {
                    id: 'port1',
                    type: 'input',
                },
                port2: {
                    id: 'port2',
                    type: 'output',
                },
            },
            properties: {
                title: 'Tod im Kleinen Tiergarten von Berlin ',
                subtitle: 'Ermittler gehen von Auftragsmord der russischen Regierung aus',
                description: 'Der Mord an einem Exil-Tschetschenen im vergangenen Sommer blieb mysteriös - bis Recherchen des SPIEGEL und von Bellingcat eine Beteiligung des russischen Geheimdienstes FSB nachwiesen. Nun klagt der Generalbundesanwalt den mutmaßlichen Täter an und benennt die Verantwortlichen. ',
                source: 'SPIEGEL',
            }
        },
        node3: {
            id: 'node3',
            type: 'document',
            position: {
                x: 100,
                y: 600,
            },
            ports: {
                port1: {
                    id: 'port1',
                    type: 'input',
                },
                port2: {
                    id: 'port2',
                    type: 'output',
                },
            },
            properties: {
                title: 'Vorwurf: Staatsterrorismus',
                subtitle: 'Mord im Kleinen Tiergarten',
                description: 'Nach monatelangen Ermittlungen zum Mord an einem Tschetschenen zeigt sich der Generalbundesanwalt überzeugt, dass Russland hinter dem Verbrechen steckt.',
                source: 'Sueddeutsche',
            }
        },
        node4: {
            id: 'node4',
            type: 'document',
            position: {
                x: 500,
                y: 600,
            },
            ports: {
                port1: {
                    id: 'port1',
                    type: 'input',
                },
                port2: {
                    id: 'port2',
                    type: 'output',
                },
            },
            properties: {
                title: 'Mord im Kleinen Tiergarten',
                subtitle: 'Mörder von Georgier hatte wohl Helfer in Berlin',
                description: 'Der Generalbundesanwalt geht offenbar davon aus, dass der Mörder von Tornike K. nicht allein handelte. Dazu sei er zu kurz in Deutschland gewesen. ',
                source: 'Zeit Online',
            }
        },
        node5: {
            id: 'node5',
            type: 'document',
            position: {
                x: 700,
                y: 200,
            },
            ports: {
                port1: {
                    id: 'port1',
                    type: 'input',
                },
                port2: {
                    id: 'port2',
                    type: 'output',
                },
            },
            properties: {
                title: ' Tiergarten-Mord ',
                subtitle: ' Russischer Auftragskiller identifiziert ',
                description: 'Wer war der Mann, der in Berlin einen Georgier erschoss? Recherchen des SPIEGEL und seiner Kooperationspartner belegen: Es war ein zuvor international gesuchter Mörder, der womöglich im Auftrag des Kreml handelte. ',
                source: 'SPIEGEL',
            }
        },
    },
    links: {
        link1: {
            id: 'link1',
            from: {
                nodeId: 'moabit',
                portId: 'port1',
            },
            to: {
                nodeId: 'node2',
                portId: 'port1',
            },
            properties: {
                label: 'Background',
            },
        },
        link2: {
            id: 'link2',
            from: {
                nodeId: 'node2',
                portId: 'port2',
            },
            to: {
                nodeId: 'node3',
                portId: 'port1',
            },
            properties: {
                label: 'Comparison',
            },
        },
        link3: {
            id: 'link3',
            from: {
                nodeId: 'node2',
                portId: 'port2',
            },
            to: {
                nodeId: 'node4',
                portId: 'port1',
            },
        },
        link4: {
            id: 'link4',
            from: {
                nodeId: 'node2',
                portId: 'port2',
            },
            to: {
                nodeId: 'node5',
                portId: 'port1',
            },
        },
    },
    selected: {},
    hovered: {},
};

