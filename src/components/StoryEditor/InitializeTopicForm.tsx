import React from "react";
import {emptyChart} from './misc/exampleChartState';
import {cloneDeep} from "lodash";
import {Button, MenuItem, Card, Overlay, Elevation} from "@blueprintjs/core";
import {Select} from "@blueprintjs/select";

import './StoryEditor.scss';

import * as Topics from "./topics";
import {
    areTopicsEqual,
    createTopic,
    ITopic,
    maybeAddCreatedTopicToArrays,
    maybeDeleteCreatedTopicFromArrays,
    renderCreateTopicOption, topicSelectProps
} from "./topics";
import {TOPICS} from "./misc/data";
import {topicPorts} from "./stories";

const TopicSelect = Select.ofType<Topics.ITopic>();

type ChartChangeCallback = (changedChart: any) => void;

class InitializeTopicForm extends React.Component<{ handleChartChange: ChartChangeCallback}, { handleChartChange: ChartChangeCallback, isOpen: boolean, topic: ITopic, items: ITopic[], createdItems: ITopic[], disableItems: ITopic[]}> {
    constructor(props: any) {
        super(props);

        const { handleChartChange } = props;
        // this.handleChartChange = handleChartChange;
        this.state = {
            isOpen: false,
            topic: TOPICS[0],
            items: topicSelectProps.items,
            createdItems: [],
            disableItems: [],
            handleChartChange: handleChartChange,
        };

        this.toggleOverlay = this.toggleOverlay.bind(this);
    }

    private toggleOverlay() {
        this.setState({
            isOpen: !this.state.isOpen,
        })
    }

    private isItemDisabled = (topic: ITopic) => this.state.disableItems && topic.rank && topic.rank > 2000;

    private handleValueChange = (topic: ITopic) => {
        // Delete the old film from the list if it was newly created.
        const { createdItems, items } = maybeDeleteCreatedTopicFromArrays(
            this.state.items,
            this.state.createdItems,
            this.state.topic,
        );
        // Add the new film to the list if it is newly created.
        const { createdItems: nextCreatedItems, items: nextItems } = maybeAddCreatedTopicToArrays(
            items,
            createdItems,
            topic,
        );

        let newChart = cloneDeep(emptyChart);
        //
        const topicId = topic.id;
        let topicNode = {
            id: topicId,
            type: 'topic',
            position: {
                x: 0,
                y: 0,
            },
            ports: topicPorts,
            properties: {
                title: topic.title,
                description: topic.description,
            }
        };
        newChart.nodes[topicId] = topicNode;

        this.state.handleChartChange(newChart);

        this.setState({ createdItems: nextCreatedItems, topic, items: nextItems, isOpen: false});
    };

    render() {
        const maybeCreateNewItemFromQuery = createTopic;
        const maybeCreateNewItemRenderer = renderCreateTopicOption;

        return (
            <div>
                <Button
                    icon="add"
                    onClick={this.toggleOverlay}
                    text="Initialize with Topic"
                />
                <Overlay
                    isOpen={this.state.isOpen}
                    canOutsideClickClose={true}
                    canEscapeKeyClose={true}
                    autoFocus={true}
                    hasBackdrop={true}
                    usePortal={true}
                    onClose={this.toggleOverlay}
                >
                    <Card
                        className="add-topic-card"
                        interactive={true}
                        elevation={Elevation.TWO}
                    >
                        <h2>Initialize with Topic</h2>

                        <TopicSelect
                            {...topicSelectProps}
                            items={this.state.items}
                            // itemDisabled={this.isItemDisabled}
                            onItemSelect={this.handleValueChange}
                            // initialContent={<MenuItem disabled={true} text={`${100} items loaded.`} />}
                            noResults={<MenuItem disabled={true} text="No results." />}
                            itemsEqual={areTopicsEqual}
                            createNewItemFromQuery={maybeCreateNewItemFromQuery}
                            createNewItemRenderer={maybeCreateNewItemRenderer}
                            popoverProps={{ minimal: false }}
                        >
                            <Button
                                icon="flag"
                                rightIcon="caret-down"
                                text="Please select a topic"
                            />
                        </TopicSelect>
                    </Card>
                </Overlay>
            </div>
        );
    }
}


export default InitializeTopicForm;
