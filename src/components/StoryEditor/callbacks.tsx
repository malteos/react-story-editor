import {IOnCanvasDrop, IStateCallback} from "@mrblenny/react-flow-chart/src/types/functions";
import {IChart} from "@mrblenny/react-flow-chart/src";
import {ILink} from "@mrblenny/react-flow-chart/src/types/chart";
import {
    onCanvasClick,
    onDeleteKey,
    onDragCanvas,
    onDragCanvasStop,
    onDragNode,
    onDragNodeStop,
    onLinkCancel,
    onLinkClick,
    onLinkComplete,
    onLinkMouseEnter,
    onLinkMouseLeave,
    onLinkMove,
    onLinkStart,
    onNodeClick,
    onNodeDoubleClick,
    onNodeMouseEnter,
    onNodeMouseLeave,
    onNodeSizeChange,
    onPortPositionChange, onZoomCanvas
} from "@mrblenny/react-flow-chart/src/container/actions";

export const onCanvasDropCustom: IStateCallback<IOnCanvasDrop> = ({
                                                                    config,
                                                                    data,
                                                                    position,
                                                                    id,
                                                                }) => (chart: IChart): IChart => {

    // Insert new node
    chart.nodes[id] = {
        id,
        position:
            config && config.snapToGrid
                ? {
                    x: Math.round(position.x / 20) * 20,
                    y: Math.round(position.y / 20) * 20,
                }
                : { x: position.x, y: position.y },
        orientation: data.orientation || 0,
        type: data.type,
        ports: data.ports,
        properties: Object.assign({rank: Object.entries(chart.nodes).length}, data.properties),
    };

    // Insert links to existing nodes
    Object.entries(chart.nodes).slice(1, 4).forEach(([toIdx, toNode])  => {
        if(toNode.id !== id && toNode.type !== 'topic') {
            const linkId: string = id + '_' + toNode.id;
            const link: ILink = {
                id: linkId,
                from: {
                    nodeId: id,
                    portId: 'port2'
                },
                to: {
                    nodeId: toNode.id,
                    portId: 'port1',
                },
                properties: {
                    label: data.properties.label
                }
            };
            chart.links[linkId] = link;
        }
    });


    console.log('CUSTOM DROP');



    return chart
};


export const customActions = {
    onCanvasDrop: onCanvasDropCustom,
    onDragNode,
    onDragNodeStop,
    onDragCanvas,
    onDragCanvasStop,
    onLinkStart,
    onLinkMove,
    onLinkComplete,
    onLinkCancel,
    onLinkMouseEnter,
    onLinkMouseLeave,
    onLinkClick,
    onCanvasClick,
    onNodeMouseEnter,
    onNodeMouseLeave,
    onDeleteKey,
    onNodeClick,
    onNodeDoubleClick,
    onNodeSizeChange,
    onPortPositionChange,
    // onCanvasDrop,
    onZoomCanvas,
};
