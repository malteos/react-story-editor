import { MenuItem } from "@blueprintjs/core";
import { ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import * as React from "react";
import {TOPICS} from "./misc/data";

export interface ITopic {
    id: string,
    /** Title of topic. */
    title: string;
    /** Description . */
    description: string;
    /** ranking. */
    rank: number;
}

export const renderTopic: ItemRenderer<ITopic> = (topic, { handleClick, modifiers, query }) => {
    if (!modifiers.matchesPredicate) {
        return null;
    }
    const text = `${topic.rank}. ${topic.title}`;
    return (
        <MenuItem
            active={modifiers.active}
            disabled={modifiers.disabled}
            // label={topic.description ? topic.description.toString(): ''}
            key={topic.rank}
            onClick={handleClick}
            text={highlightText(text, query)}
        />
    );
};

export const renderCreateTopicOption = (
    query: string,
    active: boolean,
    handleClick: React.MouseEventHandler<HTMLElement>,
) => (
    <MenuItem
        icon="add"
        text={`Create "${query}"`}
        active={active}
        onClick={handleClick}
        shouldDismissPopover={false}
    />
);

export const filterTopic: ItemPredicate<ITopic> = (query, topic, _index, exactMatch) => {
    const normalizedTitle = topic.title.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
        return normalizedTitle === normalizedQuery;
    } else {
        return `${topic.rank}. ${normalizedTitle} ${topic.description}`.indexOf(normalizedQuery) >= 0;
    }
};

function highlightText(text: string, query: string) {
    let lastIndex = 0;
    const words = query
        .split(/\s+/)
        .filter(word => word.length > 0)
        .map(escapeRegExpChars);
    if (words.length === 0) {
        return [text];
    }
    const regexp = new RegExp(words.join("|"), "gi");
    const tokens: React.ReactNode[] = [];
    while (true) {
        const match = regexp.exec(text);
        if (!match) {
            break;
        }
        const length = match[0].length;
        const before = text.slice(lastIndex, regexp.lastIndex - length);
        if (before.length > 0) {
            tokens.push(before);
        }
        lastIndex = regexp.lastIndex;
        tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
    }
    const rest = text.slice(lastIndex);
    if (rest.length > 0) {
        tokens.push(rest);
    }
    return tokens;
}

function escapeRegExpChars(text: string) {
    // return text.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return text.replace(/([.*+?^=!:${}()|[]\/\\])/g, "\\$1");

}

export const topicSelectProps = {
    itemPredicate: filterTopic,
    itemRenderer: renderTopic,
    items: TOPICS,
};

export function createTopic(title: string): ITopic {
    return {
        rank: 100 + Math.floor(Math.random() * 100 + 1),
        title,
        id: title.toLowerCase(),
        description: title,
    };
}

export function areTopicsEqual(topicA: ITopic, topicB: ITopic) {
    // Compare only the titles (ignoring case) just for simplicity.
    return topicA.title.toLowerCase() === topicB.title.toLowerCase();
}

export function doesTopicEqualQuery(topic: ITopic, query: string) {
    return topic.title.toLowerCase() === query.toLowerCase();
}

export function arrayContainsTopic(topics: ITopic[], topicToFind: ITopic): boolean {
    return topics.some((topic: ITopic) => topic.title === topicToFind.title);
}

export function addTopicToArray(topics: ITopic[], topicToAdd: ITopic) {
    return [...topics, topicToAdd];
}

export function deleteTopicFromArray(topics: ITopic[], topicToDelete: ITopic) {
    return topics.filter(topic => topic !== topicToDelete);
}

export function maybeAddCreatedTopicToArrays(
    items: ITopic[],
    createdItems: ITopic[],
    topic: ITopic,
): { createdItems: ITopic[]; items: ITopic[] } {
    const isNewlyCreatedItem = !arrayContainsTopic(items, topic);
    return {
        createdItems: isNewlyCreatedItem ? addTopicToArray(createdItems, topic) : createdItems,
        // Add a created topic to `items` so that the topic can be deselected.
        items: isNewlyCreatedItem ? addTopicToArray(items, topic) : items,
    };
}

export function maybeDeleteCreatedTopicFromArrays(
    items: ITopic[],
    createdItems: ITopic[],
    topic: ITopic,
): { createdItems: ITopic[]; items: ITopic[] } {
    const wasItemCreatedByUser = arrayContainsTopic(createdItems, topic);

    // Delete the item if the user manually created it.
    return {
        createdItems: wasItemCreatedByUser ? deleteTopicFromArray(createdItems, topic) : createdItems,
        items: wasItemCreatedByUser ? deleteTopicFromArray(items, topic) : items,
    };
}
