import * as React from 'react'
import { INode, REACT_FLOW_CHART } from '@mrblenny/react-flow-chart/src'
import {Tag} from "@blueprintjs/core";

export interface IRecommendationItem {
    title: string,
    subtitle: string,
    description: string,
    source: string,
}

export const RecommendationItem = ({ title, subtitle, description, source }: IRecommendationItem) => {
    const properties = {
        title: title,
        subtitle: subtitle,
        description: description,
        source: source
    };
    const type = 'document';
    const ports: INode['ports'] = {
        port1: {
            id: 'port1',
                type: 'top',
                properties: {
                custom: 'property',
            },
        },
        port2: {
            id: 'port1',
                type: 'bottom',
                properties: {
                custom: 'property',
            },
        },
    };

    return (
        <div className="recommendation-item"
            draggable={true}
            onDragStart={ (event) => {
                event.dataTransfer.setData(REACT_FLOW_CHART, JSON.stringify({ type, ports, properties }))
            } }
        >
            <div>
                <Tag>Document</Tag>
                <h2>{title}</h2>
                <h3>{subtitle}</h3>
                <p>{description}</p>
                {/*<Button*/}
                {/*    text="Add"*/}
                {/*    onClick={(event: any) => {*/}
                {/*        event.dataTransfer.setData(REACT_FLOW_CHART, JSON.stringify({ type, ports, properties }))*/}
                {/*    }}*/}
                {/*/>*/}
            </div>
        </div>
    )
};
