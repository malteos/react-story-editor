import {ILink} from "@mrblenny/react-flow-chart/src";
import React from "react";
import {Button, Tag} from "@blueprintjs/core";

type ChartChangeCallback = (changedChart: any) => void;

export interface ISelectedLink {
    linkId: string | undefined,
    stateActions: any,
    handleChartChange: ChartChangeCallback,
    link: ILink,
}

export const SelectedLink = ({ linkId, stateActions, handleChartChange, link }: ISelectedLink) => {
    console.log(handleChartChange);

    return (
        <div className="selected-link-wrapper">
            <div className="selected-link" data-id={linkId}>
                <Tag>Link</Tag>
                <div className="row">
                    <label>From</label>
                    <span className="value">{link.from.nodeId}</span>
                </div>
                <div className="row">
                    <label>To</label>
                    <span className="value">{link.to.nodeId}</span>
                </div>

                <div className="row">
                    <label>Relation</label>
                    <span className="value">{link.properties && link.properties.label}</span>
                </div>

                <Button
                    icon="delete"
                    intent="danger"
                    text="Delete"
                    onClick={() => {
                        const config = { readonly: false };
                        stateActions.onDeleteKey({config})
                    }}/>
            </div>
            {/*<pre>{JSON.stringify(link)}</pre>*/}
        </div>
    )
};
