import styled from 'styled-components'
import {ICanvasOuterDefaultProps} from "@mrblenny/react-flow-chart/src";

export const CanvasOuterCustom = styled.div<ICanvasOuterDefaultProps>`
  position: relative;
  background-size: 20px 20px;
  background-color: rgba(0,0,0,0.08);
  background-image:
    linear-gradient(90deg,hsla(0,0%,100%,.2) 1px,transparent 0),
    linear-gradient(180deg,hsla(0,0%,100%,.2) 1px,transparent 0);
  width: 200%;
  overflow: hidden;
  cursor: not-allowed;
` as any;
