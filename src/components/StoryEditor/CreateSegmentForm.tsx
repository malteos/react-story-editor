import React from "react";
import {Button, Card, Elevation, FormGroup, Overlay, TextArea} from "@blueprintjs/core";
import {IChart} from "@mrblenny/react-flow-chart/src";
import {defaultPorts} from "./stories";

type ChartChangeCallback = (changedChart: any) => void;

class CreateSegmentForm extends React.Component<{handleChartChange: ChartChangeCallback, chart: IChart}, {handleChartChange: ChartChangeCallback, chart: IChart, isOpen: boolean, content: string}> {
    constructor(props: any) {
        super(props);

        const { handleChartChange, chart } = props;
        // this.chart = chart;

        this.state = {
            isOpen: false,
            content: '',
            handleChartChange: handleChartChange,
            chart: chart,
        };
        this.submit = this.submit.bind(this);
    }

    private submit(event: any) {
        if(event) event.preventDefault();

        if(this.state.content === '') {
            alert('Please provide a content text.');
        }
        const changedChart = {...this.state.chart};
        const nodeId: string = 'custom-' + (Object.entries(this.state.chart.nodes).length + 1);

        changedChart.nodes[nodeId] = {
            id: nodeId,
            position: {
                x: 500,
                y: 200,
            },
            orientation: 0,
            type: 'segment',
            ports: defaultPorts,
            properties: {
                rank: Object.entries(this.state.chart.nodes).length + 1,
                content: this.state.content,
                source: 'Custom',
            },
        };
        this.setState({
            isOpen: false,
            content: '',
        });
        this.state.handleChartChange(changedChart);
    }

    render() {
        return (
            <>
                <Button
                    icon="document"
                    text="Create segment"
                    onClick={() => this.setState({isOpen: true})}
                />
                <Overlay
                    isOpen={this.state.isOpen}
                    canOutsideClickClose={true}
                    canEscapeKeyClose={true}
                    autoFocus={true}
                    hasBackdrop={true}
                    usePortal={true}
                    onClose={() => this.setState({isOpen: false})}
                >
                    <Card
                        className="create-segment-card"
                        interactive={true}
                        elevation={Elevation.TWO}
                    >
                        <h2>Create segment</h2>
                        <form onSubmit={this.submit}>
                            <FormGroup
                                helperText="Please enter a text for your custom segment."
                                label="Content"
                                labelFor="text-input"
                                labelInfo="(required)"
                            >
                                <TextArea id="contentInput" rows={7} cols={100} value={this.state.content} onChange={e => {
                                    this.setState({content: e.target.value});
                                }}/>
                            </FormGroup>
                            <Button intent="primary" icon="floppy-disk" text="Submit" type="submit"/>
                        </form>

                    </Card>
                </Overlay>
            </>
        );
    }
}


export default CreateSegmentForm;
